<html>
<body>
	<?php
		function get_rows() {
			$file=fopen("Members.txt",'r');
			while($line = fgets($file)){//while we can still read the file
				$line=trim($line);//remove the line endings and extra white-spaces at start and end
				list($Member,$Institute) = explode('|',$line);//you get an array of 4 per line, first item is author, etc...
				echo "<tr><td>$Member</td><td>$Institute</td></tr>\n";
			}
			return true;
		}
	?>
	
	<table>
		<tr>
			<th>Member</th>
			<th>Institute</th>
		</tr>
		<?php get_rows(); ?>
	</table>
</body>
</html>